'use strict';

const spawner = {
	generate: require('spawner.generate'),
	clear: require('spawner.clear'),
	attacker: require('spawner.attacker')
};

module.exports = spawner;
