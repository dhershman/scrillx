
'use strict';
try {
	const lib = require('lib');

	const actions = ['heal', 'attack', 'repair'];

	const t = Game.getObjectById('5806ee809a2b16f360260bdf');


		const tower = () => {
			if (t) {
				t.memory = {};
				t.memory.actions = actions;
				t.memory.minEnergy = 250;
				lib.actionRunner(t);
			}
		};
		module.exports = tower;
} catch(e) {
	console.log('Something went wrong in towers: ', e);
}
