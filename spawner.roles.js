
'use strict';

/**
 * Contains functions needed for automatically spawning creeps.
 * @memberOf spawner
 * @module spawner.roles
 */
const roles = {
	harvester: {
		min: 2,
		body: [WORK, CARRY, MOVE],
		actions: ['transfer'],
		priority: [STRUCTURE_SPAWN, STRUCTURE_EXTENSION, STRUCTURE_CONTAINER, STRUCTURE_TOWER],
		container: false,
		role: 'harvester'
	},
	upgrader: {
		min: 2,
		body: [WORK, CARRY, MOVE],
		actions: ['upgrade'],
		priority: [],
		container: true,
		role: 'upgrader'
	},
	builder: {
		min: 2,
		body: [WORK, CARRY, MOVE],
		actions: ['build', 'repair'],
		priority: [],
		container: true,
		role: 'builder'
	},
	repairer: {
		min: 1,
		body: [WORK, CARRY, MOVE],
		actions: ['repair', 'refill', 'build'],
		priority: [],
		container: true,
		role: 'repairer'
	},
	runner: {
		min: 0,
		body: [WORK, WORK, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE],
		actions: ['refill'],
		priority: [],
		container: true,
		role: 'runner'
	},
	attacker: {
		min: 0,
		body: [MOVE, MOVE, ATTACK, ATTACK, ATTACK, TOUGH, TOUGH],
		actions: ['attack'],
		priority: [],
		container: true,
		role: 'attacker'
	},
	healer: {
		min: 0,
		body: [MOVE, HEAL],
		actions: ['heal'],
		priority: [],
		container: true,
		role: 'healer'
	}
};

module.exports = roles;
