'use strict';

const refill = (creep) => {

  const towers = creep.room.find(FIND_STRUCTURES, {
    filter: (structure) => {
      return (structure.structureType == STRUCTURE_TOWER) && _.sum(structure.store) < 250;
    }
  });

  if (towers.length) {
    if (creep.transfer(towers[0], RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
      creep.moveTo(towers[0]);
    }
  }
}

module.exports = refill;
