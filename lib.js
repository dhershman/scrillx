'use strict';

const actions = require('action');

Creep.prototype.setAction = function() {
	if (this.memory.action && this.carry.energy == 0) {
		this.memory.action = false;
	}
	if (!this.memory.action && this.carry.energy == this.carryCapacity){
		this.memory.action = true;
	}

	if (this.memory.action) {
		this.actionRunner();
	}
	else {
		this.harvestNearestResource();
	}
};

Creep.prototype.harvestNearestResource = function() {
	if (this.memory.container){
		const con = this.room.find(FIND_STRUCTURES, {
			filter: (structure) => {
				return (structure.structureType == STRUCTURE_CONTAINER) && _.sum(structure.store) > 0;
			}
		});
		if (con.length){
			const c = _.sortBy(con, 'store');
			if (this.withdraw(c[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE){
				this.moveTo(c[0]);
			}
			return;
		}
	}
	const source = this.pos.findClosestByRange(FIND_SOURCES);
	if (this.harvest(source) == ERR_NOT_IN_RANGE) {
		this.moveTo(source);
	}
};

Creep.prototype.actionRunner = function() {
	const priority = this.memory.actions;
	for (let i = 0, len = priority.length; i < len; i++){
		if (actions[priority[i]](this)){
			return;
		}
	}
};
