'use strict';

const action = {
	build: require('action.build'),
	repair: require('action.repair'),
	transfer: require('action.transfer'),
	upgrade: require('action.upgrade'),
	attack: require('action.attack'),
	heal: require('action.heal'),
	refill: require('action.refill')
};

module.exports = action;
